import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {CardColumns} from "reactstrap";

import ListItem from "../../components/ListItem/ListItem";
import {fetchAllPhotos, fetchPhotosByAuthor} from "../../store/actions/photos";

class AllItems extends Component {
  state = {
    show: false
  };

  handleHide() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  componentDidMount() {
    if (this.props.location.search === '') {
      this.props.fetchAllPhotos();
    }
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      console.log(nextProps.match.params.id);
      if (nextProps.match.params.id) {
        this.props.fetchPhotosByAuthor(nextProps.match.params.id);
      } else {
        this.props.fetchAllPhotos();
      }
    }
  }

  render() {
    return (
      <Fragment>
        <CardColumns>
          {this.props.photos && this.props.photos.map(photo => {
            return (
              <Fragment>
                <ListItem
                  key={photo._id}
                  title={photo.title}
                  image={photo.image}
                  author={photo.author.display_name}
                  id={photo._id}
                  show={() => this.handleShow(photo._id)}
                  close={() => this.handleHide(photo._id)}
                  state={this.state.show}
                  contain={this}
                />
              </Fragment>
            );
          })}
        </CardColumns>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos.photos,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchAllPhotos: () => dispatch(fetchAllPhotos()),
    fetchPhotosByAuthor: id => dispatch(fetchPhotosByAuthor(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllItems);