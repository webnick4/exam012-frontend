import React, {Fragment} from "react";
import {Card, CardBody, CardImg, CardText, CardTitle, Col} from "reactstrap";
import PropTypes from "prop-types";
import config from "../../config";

import notFound from "../../assets/images/not-found.png";
import {Button, Image, Modal, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const ListItem = props => {
  let image = notFound;

  if (props.image) {
    image = config.apiUrl + "uploads/" + props.image;
  }

  return (
    <Fragment>
      <Modal
        show={props.state}
        onHide={props.close}
        container={props.contain}
        aria-labelledby="contained-modal-title"
      >
        <Modal.Body>
          {props.image ? (
          <Image src={image} style={{width: "100%"}} />
          ) : null}
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.close}>Close</Button>
        </Modal.Footer>
      </Modal>

      <Col sm="4" key={props.id}>
        <Card style={{marginBottom: '10px'}}>
          {props.image !== "hide" ? (
            <CardImg
              top
              width="100%"
              src={image}
              alt="Product image"
              style={{ width: "85%", marginRight: "2%" }}
              onClick={props.show}
            />
          ) : null}
          <CardBody style={{paddingBottom: 25}}>
            <CardTitle tag="h3" onClick={props.click}>
              {props.title}
            </CardTitle>
            <CardText>
              <LinkContainer to={`/authors/${props.id}`} exact>
                <NavItem>{props.author}</NavItem>
              </LinkContainer>
            </CardText>
          </CardBody>
        </Card>
      </Col>
    </Fragment>
  );
};

ListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  image: PropTypes.string,
  click: PropTypes.func,
  show: PropTypes.func,
  close: PropTypes.func,
  contain: PropTypes.object,
  state: PropTypes.bool
};

export default ListItem;