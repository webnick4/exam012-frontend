import React, { Component } from 'react';

import Layout from "./containers/Layout/Layout";
import {Route, Switch} from "react-router-dom";

import AllItems from "./containers/AllItems/AllItems";
import Register from "./containers/Auth/Register/Register";
import Login from "./containers/Auth/Login/Login";


class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={AllItems} />
          <Route path="/authors/:id" exact component={AllItems} />
          <Route path="/register" exact component={Register} />
          <Route path="/login" exact component={Login} />
          <Route render={() => <h1 style={{textAlign: 'center'}}>Page not found</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;