import axios from '../../axios-api';
import {
  FETCH_PHOTOS_BY_AUTHOR_FAILURE, FETCH_PHOTOS_BY_AUTHOR_SUCCESS, FETCH_PHOTOS_FAILURE,
  FETCH_PHOTOS_SUCCESS
} from "./actionTypes";

const fetchPhotosSuccess = photos => {
  return {type: FETCH_PHOTOS_SUCCESS, photos};
};

const fetchPhotosFailure = error => {
  return {type: FETCH_PHOTOS_FAILURE, error};
};

export const fetchAllPhotos = () => {
  return dispatch => {
    return axios.get('/photos').then(
      response => {
        dispatch(fetchPhotosSuccess(response.data));
      },
      error => {
        dispatch(fetchPhotosFailure(error));
      }
    );
  };
};

const fetchPhotosByAuthorSuccess = photos => {
  return {type: FETCH_PHOTOS_BY_AUTHOR_SUCCESS, photos};
};

const fetchPhotosByAuthorFailure = error => {
  return {type: FETCH_PHOTOS_BY_AUTHOR_FAILURE, error};
};

export const fetchPhotosByAuthor = id => {
  return dispatch => {
    return axios.get(`/photos/authors/${id}`).then(
      response => {
        dispatch(fetchPhotosByAuthorSuccess(response.data));
      },
      error => {
        dispatch(fetchPhotosByAuthorFailure(error));
      }
    );
  };
};