import {
  FETCH_PHOTOS_BY_AUTHOR_FAILURE, FETCH_PHOTOS_BY_AUTHOR_SUCCESS, FETCH_PHOTOS_FAILURE,
  FETCH_PHOTOS_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  photos: [],
  item: [],
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PHOTOS_SUCCESS:
      return {...state, photos: action.photos};
    case FETCH_PHOTOS_FAILURE:
      return {...state, error: action.error};
    case FETCH_PHOTOS_BY_AUTHOR_SUCCESS:
      return {...state, products: action.products, error: null};
    case FETCH_PHOTOS_BY_AUTHOR_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;